﻿namespace XamarinFormsDataBinding
{

    public class AuthDataModel
    {
        public string Account { get; set; }
        public string Passwrord { get; set; }
        public bool Valid()
        {
            return !(string.IsNullOrEmpty(this.Account) || string.IsNullOrEmpty(this.Passwrord));
        }

        public void Login() { }
    }

}
