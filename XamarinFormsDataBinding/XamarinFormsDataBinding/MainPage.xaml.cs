﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace XamarinFormsDataBinding
{
    public partial class MainPage : ContentPage
    {

        public MainPage()
        {
            InitializeComponent();
            AuthDataModel model = new AuthDataModel();
            this.BindingContext = model;
            this.btnLogin.Clicked += (sender, e) =>
            {
                if (model.Valid())
                    model.Login();
                else
                    this.DisplayAlert("提示", "帳號或密碼未填", "OK");
            };
        }

    }
}
